# need

keras == 2.1.2#2.2.4
tensorflow-gpu == 1.2.0


# Five datasets for singing voice detection

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2641106.svg)](https://doi.org/10.5281/zenodo.2641106)


### preprocess.py

数据预处理，格式化数据，单声道|16000Hz，歌声伴奏分离

### extractFeature.py

获取组合特征combine
combineFeat = get_audio_feature()


* temporal features: 

computed from the audio signal frame (zero-crossing rate and linear prediction coefficients)

* energy features: 

referring to the energy content of the signal (root mean square energy of the signal frame)

* spectral shape features: 

describing the shape of the power spectrum of a signal frame: centroid, spread, skewness, kurtosis, slope, roll-off frequency, variation, Mel-frequency cepstral coefficients (MFCCs)

* perceptual features: 

computed using a model of the human earring process (relative specific loudness, sharpness, and spread).

featMFCC 26
featChroma 12
featZCR 1
featRMSE 1
featCentroid 1
featRolloff 1
featPoly 2
featContrast 7
featBandwith 1
featFlatness 1
featBandwith 1
feat_mfcc_delta_delta 39
feat_mfcc_delta 26
feat_plp 9
featLPCC 12

