# coding=utf-8
import csv
import os

import joblib
import numpy
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.layers import Dense, Conv1D, MaxPooling1D, Flatten, LSTM, GRU, Bidirectional, TimeDistributed, \
    Dropout, Reshape, ConvLSTM2D, MaxPooling2D
from keras.models import Sequential
from keras.models import load_model
from keras.optimizers import Adam
from keras.utils import plot_model
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.utils import shuffle as shuffle_ds
from xgboost import XGBClassifier

from classify_util import Evaluator_Parm
from config import EARLY_STOP_STEP, \
    tb_log_dir, cp_save_path, VERBOSE, SEED, dropout_rate
from dataset_util import Dataset_save_gen
from extract_feat import AudioFeat


def use_cnn(input_size):
    model = Sequential()
    model.add(Reshape((input_size, 1), input_shape=(input_size,)))
    model.add(
        Conv1D(64, 3, name='conv1', padding='valid', kernel_initializer='he_normal', activation='relu'
               ))
    # model.add(ReLU(0.01))
    model.add(Conv1D(32, 3, name='conv2', padding='valid', kernel_initializer='he_normal', activation='relu'))
    # model.add(ReLU(0.01))
    model.add(MaxPooling1D(3, strides=3))

    model.add(Conv1D(128, 3, name='conv3', padding='valid', kernel_initializer='he_normal', activation='relu'))
    # model.add(ReLU(0.01))
    model.add(Conv1D(64, 3, name='conv4', padding='valid', kernel_initializer='he_normal', activation='relu'))
    # model.add(ReLU(0.01))
    model.add(MaxPooling1D(3, strides=3))

    model.add(Flatten())
    model.add(Dense(256))
    model.add(Dropout(dropout_rate))
    model.add(Dense(64))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, activation='sigmoid'))
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])
    return model


def use_lstm(input_size, time_steps):
    model = Sequential()
    model.add(Bidirectional(LSTM(10, return_sequences=True), input_shape=(time_steps, input_size)))
    model.add(Bidirectional(LSTM(10, return_sequences=True)))
    model.add(Bidirectional(LSTM(10, return_sequences=True)))
    model.add(TimeDistributed(Dense(1, activation='sigmoid')))
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])
    return model


def use_gru(input_size, time_steps):
    model = Sequential()
    model.add(Bidirectional(GRU(30, return_sequences=True), input_shape=(time_steps, input_size)))
    model.add(Bidirectional(GRU(20, return_sequences=True)))
    model.add(Bidirectional(GRU(40, return_sequences=True)))
    model.add(TimeDistributed(Dense(1, activation='sigmoid')))
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])
    return model


def use_lrcn(input_size, time_steps):
    model = Sequential()
    drop_rate = 0.2
    model.add(Reshape((1, 1, time_steps, input_size), input_shape=(time_steps, input_size)))
    model.add(ConvLSTM2D(13, 1, 4, activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(drop_rate))
    model.add(Flatten())
    model.add(Dense(200, activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(Dense(50, activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(Dense(1, activation='sigmoid'))
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])

    return model


def use_dnn(input_size):
    model = Sequential()
    model.add(Dense(32, activation='relu', input_shape=(input_size,)))
    model.add(Dropout(dropout_rate))
    model.add(Dense(16, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, activation='sigmoid'))
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])
    return model


def use_xgboost_clf():
    xgb = XGBClassifier()
    parameters = {}  # done
    clf = GridSearchCV(xgb, parameters, cv=5, verbose=1, n_jobs=-1)
    return clf

def use_gbdt_clf():
    gbdt=GradientBoostingClassifier()
    parameters = {}  # done
    clf = GridSearchCV(gbdt, parameters, cv=5, verbose=1, n_jobs=-1)
    return clf
def use_random_forest_clf():
    rf = RandomForestClassifier(random_state=SEED, n_estimators=100)
    parameters = {'min_samples_leaf': [5, 10, 20, 30]}  # done
    clf = GridSearchCV(rf, parameters, cv=5, verbose=1, n_jobs=-1)
    return clf


def use_gmm_clf():
    gmm = GaussianMixture()
    parameters = {'n_components': [2]}  # done
    clf = GridSearchCV(gmm, parameters, cv=5, verbose=1, n_jobs=-1)
    return clf


def use_svm_clf():
    # clf = SVC(gamma='auto',C=1.0,kernel='linear',coef0=0.0)

    svc = LinearSVC(random_state=0, tol=1e-05, max_iter=1000)
    # parameters = {'C': [0.1,1,2,3,4,8,10,20]}
    parameters = {'C': [1]}  # done
    clf = GridSearchCV(svc, parameters, cv=5, verbose=1, n_jobs=-1)

    return clf


def get_step_data(dataX, dataY, step, array_dim_num=3):
    feat_dim = numpy.shape(dataX)[-1]
    finalX = []
    finalY = []
    for item_y in range(0, len(dataY), step):
        if list(dataY[item_y:item_y + step]).count(1) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(1)
        if list(dataY[item_y:item_y + step]).count(0) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(0)
    finalX = numpy.array(finalX)
    finalY = numpy.array(finalY)
    if array_dim_num == 4:
        finalX = numpy.reshape(finalX, (-1, 1, 1, step, feat_dim))
    return finalX, finalY


def get_step_data_lstm(dataX, dataY, step, array_dim_num=3):
    feat_dim = numpy.shape(dataX)[-1]
    finalX = []
    finalY = []
    for item_y in range(0, len(dataY), step):
        if len(dataX[item_y:item_y + step]) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(dataY[item_y:item_y + step])

    finalX = numpy.array(finalX)
    finalY = numpy.array(finalY)
    finalX = numpy.reshape(finalX, (-1, step, feat_dim))
    finalY = numpy.reshape(finalY, (-1, step, 1))
    return finalX, finalY


class Build_Mode:
    def __init__(self, dataset_path, feat_lst=None):
        if feat_lst is None:
            feat_lst = ['feat_plp', 'feat_chroma', 'feat_lpc',
                        'feat_mfcc',
                        'feat_MFCC_delta', 'feat_MFCC_delta_delta',
                        'feat_spec_all']
        ds_save_gen = Dataset_save_gen()
        self.trainX, self.trainY, self.testX, self.testY, self.validX, self.validY = ds_save_gen.load_Dataset_from_h5file(
            dataset_path)
        audio_feater = AudioFeat()
        print('load dataset into train test valid')
        self.trainX = audio_feater.specify_featue_lst_from_total_feats(self.trainX,
                                                                       feat_lst)
        self.testX = audio_feater.specify_featue_lst_from_total_feats(self.testX,
                                                                      feat_lst)
        self.validX = audio_feater.specify_featue_lst_from_total_feats(self.validX,
                                                                       feat_lst)

    def train_or_load_model(self, use_model='dnn', load=True,scale_normal=True):

        clf_models = ['svm', 'svr', 'random_forest', 'gmm', 'xgboost','gbdt']
        nn_models = ['dnn', 'cnn', 'lstm', 'gru', 'lrcn']

        if use_model == 'svm':
            model = use_svm_clf()
            train_X, train_Y = shuffle_ds(self.trainX, self.trainY)
            train_X = train_X[:2000]
            train_Y = train_Y[:2000]
            valid_X, valid_Y = self.validX, self.validY
        elif use_model == 'gmm':
            model = use_gmm_clf()
            # train_X, train_Y = self.trainX, self.trainY
            train_X, train_Y = shuffle_ds(self.trainX, self.trainY)
            train_X = train_X[:20000]
            train_Y = train_Y[:20000]
            valid_X, valid_Y = self.validX, self.validY
        elif use_model == 'random_forest':
            model = use_random_forest_clf()
            train_X, train_Y = self.trainX, self.trainY
            valid_X, valid_Y = self.validX, self.validY
        elif use_model == 'xgboost':
            model = use_xgboost_clf()
            train_X, train_Y = self.trainX, self.trainY
            valid_X, valid_Y = self.validX, self.validY
        elif use_model == 'gbdt':
            model = use_gbdt_clf()
            train_X, train_Y = self.trainX, self.trainY
            valid_X, valid_Y = self.validX, self.validY
        elif use_model == 'dnn':
            model = use_dnn(input_size=276)
            train_X, train_Y = self.trainX, self.trainY
            valid_X, valid_Y = self.validX, self.validY
            if scale_normal:
                scaler=StandardScaler()
                scaler.fit(train_X)
                train_X=scaler.transform(train_X)
                valid_X=scaler.transform(valid_X)
        elif use_model == 'cnn':
            model = use_cnn(input_size=276)
            train_X, train_Y = self.trainX, self.trainY
            valid_X, valid_Y = self.validX, self.validY
            if scale_normal:
                scaler=StandardScaler()
                scaler.fit(train_X)
                train_X=scaler.transform(train_X)
                valid_X=scaler.transform(valid_X)
        elif use_model == 'lstm':
            model = use_lstm(input_size=276, time_steps=20)
            train_X, train_Y = get_step_data_lstm(self.trainX, self.trainY, step=20)
            valid_X, valid_Y = get_step_data_lstm(self.validX, self.validY, step=20)
        elif use_model == 'gru':
            model = use_gru(input_size=276, time_steps=20)
            if scale_normal:
                scaler=StandardScaler()
                scaler.fit(self.validX)
                self.trainX=scaler.transform(self.trainX)
                self.validX=scaler.transform(self.validX)
            train_X, train_Y = get_step_data_lstm(self.trainX, self.trainY, step=20)
            valid_X, valid_Y = get_step_data_lstm(self.validX, self.validY, step=20)
        elif use_model == 'lrcn':
            model = use_lrcn(input_size=276, time_steps=20)
            if scale_normal:
                scaler=StandardScaler()
                scaler.fit(self.validX)
                self.trainX=scaler.transform(self.trainX)
                self.validX=scaler.transform(self.validX)
            train_X, train_Y = get_step_data(self.trainX, self.trainY, step=20)
            valid_X, valid_Y = get_step_data(self.validX, self.validY, step=20)
        else:
            train_X, train_Y = None, None
            valid_X, valid_Y = None, None
            model = None
            # raise ('404 error: %s model not known' % use_model)
        if use_model in nn_models:
            if load:
                model = load_model('models/%s.model' % use_model)
                model.load_weights('models/%s.weights' % use_model)
            else:
                plot_model(model, to_file='models/%s.png' % use_model, show_shapes=True)
                checkpoint = ModelCheckpoint(cp_save_path, monitor='loss', verbose=1, save_best_only=True, mode='min')
                early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=EARLY_STOP_STEP, verbose=0,
                                           mode='auto', )  # baseline=None, restore_best_weights=True)

                history = model.fit(train_X, train_Y,
                                    epochs=10000, verbose=VERBOSE,
                                    shuffle=True,
                                    validation_data=(valid_X, valid_Y),
                                    callbacks=[checkpoint, early_stop,
                                               TensorBoard(log_dir=tb_log_dir), ])
                plt.figure()
                plt.plot()
                plt.plot(history.history['val_acc'])
                plt.title('model accuracy')
                plt.ylabel('accuracy')
                plt.xlabel('epoch')
                plt.legend(['train', 'test'], loc='upper left')
                plt.savefig('%s_val_acc.png' % use_model)
                # summarize history for loss
                plt.figure()
                plt.plot(history.history['loss'])
                plt.plot(history.history['val_loss'])
                plt.title('model loss')
                plt.ylabel('loss')
                plt.xlabel('epoch')
                plt.legend(['train', 'test'], loc='upper left')
                plt.savefig('%s_val_loss.png' % use_model)

                # 已有的model在load权重过后
                # 取某一层的输出为输出新建为model，采用函数模型
                # dense1_layer_model = Model(inputs=model.input,
                #                            outputs=model.layers[-2].output)
                # 以这个model的预测值作为输出dense1_layer_model.predict()
                model.save('models/%s.model' % use_model)
                model.save_weights('models/%s.weights' % use_model)
            print('predict...')
            predict_Y = model.predict_classes(valid_X)
        else:
            print('begin fit %s model...' % use_model)
            if load:
                model = joblib.load('models/%s.model' % use_model,)
            else:
                model.fit(train_X, train_Y)
                joblib.dump(model, 'models/%s.model' % use_model,)
                print(model.best_score_)
                print(model.best_params_)
                print(model.best_estimator_)
            print('predict...')
            predict_Y = model.predict(valid_X)
        evaluator = Evaluator_Parm(predict_Y, valid_Y)

        accuracy, precision, recall, f1measure = evaluator.get_metrics_f()
        return accuracy, precision, recall, f1measure


def compare_features_by_g(dataset_h5_path):
    full_feature_sets = [
        'feat_plp', 'feat_chroma', 'feat_lpc',
        'feat_mfcc',
        'feat_MFCC_delta', 'feat_MFCC_delta_delta',
        'feat_spec_all'

    ]

    for select_feat in full_feature_sets:
        print('use feat:\t%s' % select_feat)
        use_model_builder = Build_Mode(dataset_h5_path, feat_lst=[select_feat])
        accuracy, precision, recall, f1measure = use_model_builder.train_or_load_model('random_forest',
                                                                                       load=False)

        with open('compare_features_by_g.txt', 'a') as df:
            df.write('accuracy\tprecision\trecall\tf1measure\n')
            df.write('%.3f\t%.3f\t%.3f\t%.3f\n' % (accuracy, precision, recall, f1measure))
    return 0


def select_feature_by_add_in_higher(dataset_h5_path):
    feature_lst = [
        'feat_spec_all',
        'feat_mfcc',
        'feat_MFCC_delta',
        'feat_MFCC_delta_delta',
        'feat_lpc',
        'feat_plp',
        'feat_chroma',

    ]

    now_feats = [feature_lst[0]]
    use_model_builder = Build_Mode(dataset_h5_path, feat_lst=now_feats)
    now_f1 = use_model_builder.train_or_load_model('random_forest',
                                                   load=False)[-2]
    counter_item = 1
    for item in feature_lst[1:]:
        counter_item += 1
        use_model_builder = Build_Mode(dataset_h5_path, feat_lst=now_feats + [item])
        add_item_f1 = use_model_builder.train_or_load_model('random_forest',
                                                            load=False)[-2]

        if add_item_f1 > now_f1:
            now_f1 = add_item_f1
            now_feats.append(item)
            print('++++%d %s' % (counter_item, item))
        else:
            print('----%d %s' % (counter_item, item))

    '''添加后特征集合
    'feat_spec_all',
    'feat_mfcc',
    'feat_MFCC_delta',
    'feat_MFCC_delta_delta',
    'feat_lpc',
    'feat_plp',
    '''
    print('best feats combination ...')
    print(now_feats)
    return now_feats


def use_best_feats_on_differ_win_size(dataset_dir):
    selected_feats = ['feat_spec_all',
                      'feat_mfcc',
                      'feat_MFCC_delta',
                      'feat_MFCC_delta_delta',
                      'feat_lpc',
                      'feat_plp']
    best_win = ''
    best_f1 = 0
    for h5_file_item in os.listdir(dataset_dir):
        if '.h5' in h5_file_item:
            dataset_h5_path = os.path.join(dataset_dir, h5_file_item)
            print(dataset_h5_path)
            use_model_builder = Build_Mode(dataset_h5_path, feat_lst=selected_feats)
            accuracy, precision, recall, f1measure = use_model_builder.train_or_load_model('random_forest',
                                                                                           load=False)
            if f1measure > best_f1:
                best_f1 = f1measure
                best_win = h5_file_item.split('_')[-2]
            print('winsize\t%s\t%.3f\t%.3f\t%.3f\t%.3f' % (
                h5_file_item.split('_')[-2], accuracy, precision, recall, f1measure))
    return best_win


def use_selct_feat_specify_win(dataset_h5_path):
    selected_feats = ['feat_spec_all',
                      'feat_mfcc',
                      'feat_MFCC_delta',
                      'feat_MFCC_delta_delta',
                      'feat_lpc',
                      'feat_plp']

    for model_type in ['dnn', 'cnn', 'gru', 'lrcn',]:  # , 'gmm', 'random_forest', 'xgboost','gbdt', 'lstm','svm']:
        use_model_builder = Build_Mode(dataset_h5_path, feat_lst=selected_feats)
        accuracy, precision, recall, f1measure = use_model_builder.train_or_load_model(model_type,
                                                                                       load=False)

        print('%s\t %.3f\t%.3f\t%.3f\t%.3f\n' % (model_type, accuracy, precision, recall, f1measure))
        writer.writerow([model_type, accuracy, precision, recall, f1measure])
    return 0


if __name__ == '__main__':
    res_csv = open('results.csv', 'a')
    writer = csv.writer(res_csv)

    # compare_features_by_g('../fin_data_svd/RWC/_0.05_vocal.h5')
    # selected_feats = select_feature_by_add_in_higher('../fin_data_svd/RWC/_0.05_vocal.h5')
    # use_best_feats_on_differ_win_size('../fin_data_svd/RWC')
    use_selct_feat_specify_win('../fin_data_svd/RWC/_0.50_vocal.h5')
    res_csv.close()
    pass
