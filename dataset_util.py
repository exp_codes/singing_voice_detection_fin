import os
from shutil import copy

import h5py
import joblib
import numpy
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt


class MedleyDB_processor:
    '''
    if __name__ == '__main__':
        medley = MedleyDB_processor('../../MedleyDB')
        medley.filter_out_need_wavs()
        medley.tags_transfer()
    '''

    def __init__(self, data_dir):
        self.dataset_dir = data_dir

    def filter_out_need_wavs(self):
        tags_dir = self.dataset_dir + '/Annotations/Melody_Annotations/MELODY2'
        wavs_dir = self.dataset_dir + '/Audio'
        with open('medleydb_vocal_songs.txt', 'r') as need_songs_f:
            all_need_songs = need_songs_f.readlines()
        all_song_names = [item.split('_MIX.wav')[0] for item in all_need_songs]
        for song_name in all_song_names:
            wav_path = '%s/%s/%s_MIX.wav' % (wavs_dir, song_name, song_name)
            tag_path = '%s/%s_MELODY2.csv' % (tags_dir, song_name)
            print(wav_path, tag_path)
            print(os.path.isfile(wav_path), os.path.isfile(tag_path))
            des_wav_path = 'MedleyDB/WavFiles/%s' % (os.path.basename(wav_path))
            des_tag_path = 'MedleyDB/tags/%s' % (os.path.basename(tag_path))

            if not os.path.isdir('MedleyDB/WavFiles/'):
                os.makedirs('MedleyDB/WavFiles/')
            if not os.path.isdir('MedleyDB/tags/'):
                os.makedirs('MedleyDB/tags/')
            copy(wav_path, des_wav_path)
            copy(tag_path, des_tag_path)
        return 0

    def tags_transfer(self):
        for tag_item in os.listdir('MedleyDB/tags'):
            tag_path = os.path.join('MedleyDB/tags', tag_item)
            with open(tag_path, 'r')as tag_f:
                tag_cont = tag_f.readlines()
            start_end_lable = []
            start = None
            lable = None
            for line_item in tag_cont:
                now_time, pitch_value = map(float, line_item.strip('\n').split(','))
                if pitch_value > 0:
                    now_lable = 'sing\n'
                else:
                    now_lable = 'nosing\n'
                if start == None:
                    start = now_time
                    lable = now_lable
                    end = now_time
                else:
                    if lable != now_lable:
                        end = now_time
                        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                        start = now_time
                        lable = now_lable
                    else:
                        lable = now_lable
                        end = now_time
            start_end_lable.append('%.3f %.3f %s' % (start, end, lable))

            transfer_tag_path = tag_path.replace('/tags', '/transfer_tags').replace('_MELODY2.csv', '.lab')
            transfer_tag_dir = os.path.dirname(transfer_tag_path)
            if not os.path.isdir(transfer_tag_dir):
                os.makedirs(transfer_tag_dir)
            with open(transfer_tag_path, 'w') as ttF:
                ttF.writelines(start_end_lable)
        return 0


class MIR1K_processor:
    '''
    mir1k = MIR1K_processor('../../MIR-1K/MIR-1K')
    mir1k.tags_transfer()
    '''

    def __init__(self, data_dir):
        self.dataset_dir = data_dir

    def tags_transfer(self):
        pitch_lable_dir = self.dataset_dir + "/PitchLabel"
        for tag_item in os.listdir(pitch_lable_dir):
            tag_path = os.path.join(pitch_lable_dir, tag_item)
            with open(tag_path, 'r')as tag_f:
                tag_cont = tag_f.readlines()
            start_end_lable = []
            start = None
            lable = None
            for line_item in tag_cont:
                now_time, pitch_value = map(float, line_item.strip('\t\n').split('\t'))
                if pitch_value > 0:
                    now_lable = 'sing\n'
                else:
                    now_lable = 'nosing\n'
                if start == None:
                    start = 0
                    end = now_time
                    lable = now_lable
                else:
                    if lable != now_lable:
                        end = now_time
                        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                        start = now_time
                        lable = now_lable
                    else:
                        end = now_time
                        lable = now_lable
            start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
            transfer_tag_path = tag_path.replace('/PitchLabel', '/tags').replace('.pv', '.lab')
            transfer_tag_dir = os.path.dirname(transfer_tag_path)
            if not os.path.isdir(transfer_tag_dir):
                os.makedirs(transfer_tag_dir)
            with open(transfer_tag_path, 'w') as ttF:
                ttF.writelines(start_end_lable)
        return 0


class iKala_processor:
    '''
    if __name__ == '__main__':
        ikala = iKala_processor('../../iKala')
        ikala.tags_transfer()
    '''

    def __init__(self, data_dir):
        self.dataset_dir = data_dir

    def tags_transfer(self):
        pitch_lable_dir = self.dataset_dir + "/PitchLabel"
        for tag_item in os.listdir(pitch_lable_dir):
            tag_path = os.path.join(pitch_lable_dir, tag_item)
            with open(tag_path, 'r')as tag_f:
                tag_cont = tag_f.readlines()
            start_end_lable = []
            start = None
            lable = None
            for index_item, line_item in enumerate(tag_cont):
                now_time = index_item * 30 / len(tag_cont)
                pitch_value = float(line_item.strip('\n'))
                if pitch_value > 0:
                    now_lable = 'sing\n'
                else:
                    now_lable = 'nosing\n'
                if start == None:
                    start = now_time
                    end = now_time
                    lable = now_lable
                else:
                    if lable != now_lable:
                        end = now_time
                        start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
                        start = now_time
                        lable = now_lable
                    else:
                        end = now_time
                        lable = now_lable
            start_end_lable.append('%.3f %.3f %s' % (start, end, lable))
            transfer_tag_path = tag_path.replace('/PitchLabel', '/tags').replace('.pv', '.lab')
            transfer_tag_dir = os.path.dirname(transfer_tag_path)
            if not os.path.isdir(transfer_tag_dir):
                os.makedirs(transfer_tag_dir)
            with open(transfer_tag_path, 'w') as ttF:
                ttF.writelines(start_end_lable)
        return 0


class Dataset_save_gen:
    def load_Dataset_from_h5file(self, h5file_path):
        h5_file = h5py.File(h5file_path, 'r')
        trainX = h5_file['trainX'][:]
        trainY = h5_file['trainY'][:]
        testX = h5_file['testX'][:]
        testY = h5_file['testY'][:]
        validX = h5_file['validX'][:]
        validY = h5_file['validY'][:]
        h5_file.close()
        return trainX, trainY, testX, testY, validX, validY

    def writeDataset2H5file(self, data_set_dir, data_ext_str, vocal_only):
        if vocal_only:
            h5name = os.path.join(data_set_dir, data_ext_str.replace('.joblib', '_vocal.h5'))
            data_ext_str = '.unet.Vocal' + data_ext_str
        else:
            h5name = os.path.join(data_set_dir, data_ext_str.replace('.joblib', '_mix.h5'))
        print("now is writing %s ..." % h5name)
        if os.path.isfile(h5name):
            return
        trainX = []
        trainY = []
        testX = []
        testY = []
        validX = []
        validY = []
        for root, dirs, names in os.walk(data_set_dir):
            for name_item in names:
                if data_ext_str in name_item:
                    if not vocal_only:
                        if '.unet.Vocal' in name_item:
                            continue
                    file_path = os.path.join(root, name_item)
                    data_lable_dict = joblib.load(file_path)
                    data = data_lable_dict['data']
                    lable = data_lable_dict['lable']
                    if '/train/' in file_path:
                        trainX.extend(data)
                        trainY.extend(lable)
                    elif '/test/' in file_path:
                        testX.extend(data)
                        testY.extend(lable)
                    elif '/valid/' in file_path:
                        validX.extend(data)
                        validY.extend(lable)
                    else:
                        raise ('error train_test_valid')

        trainX = np.vstack(trainX)
        testX = np.vstack(testX)
        validX = np.vstack(validX)
        trainY = np.array(trainY)
        testY = np.array(testY)
        validY = np.array(validY)
        file = h5py.File(h5name, 'w')
        file.create_dataset('trainX', data=trainX)
        file.create_dataset('trainY', data=trainY)
        file.create_dataset('testX', data=testX)
        file.create_dataset('testY', data=testY)
        file.create_dataset('validX', data=validX)
        file.create_dataset('validY', data=validY)
        file.close()
        return 0

    def batch_data_gen(self, data_set_dir, data_ext_str, batch_size=50, feat_selc=None, time_steps=30):
        if time_steps != 1:
            x = np.zeros((batch_size, time_steps, 288), dtype=np.uint8)
            y = np.zeros((batch_size, time_steps, 1), dtype=np.uint8)
            time_steps_buffer = []
            time_steps_y = []
            while True:
                batch_size_item = 0
                for root, dirs, names in os.walk(data_set_dir):
                    for name_item in names:
                        if data_ext_str in name_item:
                            file_path = os.path.join(root, name_item)
                            data_lable_dict = joblib.load(file_path)
                            data = data_lable_dict['data']
                            lable = data_lable_dict['lable']
                            for item in range(0, len(data)):
                                Y_item = numpy.array(lable[item])
                                if batch_size_item < batch_size:
                                    if len(time_steps_buffer) < time_steps:
                                        time_steps_buffer.append(data[item])
                                        time_steps_y.append(Y_item)
                                    else:
                                        x[batch_size_item] = numpy.vstack(time_steps_buffer)
                                        y[batch_size_item] = numpy.vstack(time_steps_y)
                                        time_steps_buffer = [data[item]]
                                        time_steps_y = [Y_item]
                                        batch_size_item += 1
                                else:
                                    batch_size_item = 0
                                    xx = x[:, :, feat_selc]
                                    # xx = xx / 1.0 / xx.max(axis=0)
                                    # xx = numpy.nan_to_num(xx)
                                    yield xx, y
        else:
            x = np.zeros((batch_size, 288), dtype=np.uint8)
            y = np.zeros((batch_size), dtype=np.uint8)
            while True:
                batch_size_item = 0
                for root, dirs, names in os.walk(data_set_dir):
                    for name_item in names:
                        if data_ext_str in name_item:
                            file_path = os.path.join(root, name_item)
                            data_lable_dict = joblib.load(file_path)
                            data = data_lable_dict['data']
                            lable = data_lable_dict['lable']

                            for item in range(0, len(data)):
                                Y_item = numpy.array(lable[item])
                                if batch_size_item < batch_size:
                                    x[batch_size_item] = data[item]
                                    y[batch_size_item] = Y_item
                                    batch_size_item += 1
                                else:
                                    batch_size_item = 0
                                    xx = x[:, feat_selc]
                                    # xx = xx / 1.0 / xx.max(axis=0)
                                    # xx = numpy.nan_to_num(xx)
                                    # xx = normalize(xx, axis=0, norm='max')
                                    yield xx, y

    def plot_dataset(self, data_x, lable_y, dim_y):
        sns.set(style="white", color_codes=True)
        df = pd.DataFrame(data_x, columns=['dim_' + str(dim_item) for dim_item in range(data_x.shape[1])])
        df["res_label"] = pd.Series(lable_y)  # .apply(lambda x: "sing" if x == 1 else "nosing")
        sns.catplot(x='res_label', y='dim_' + str(dim_y), data=df, hue="res_label", kind='box')

        plt.savefig('dataset_%s_jamendo_plot.png' % (str(dim_y)))

        return 0

    def view_label_resolution(self, data_dir):
        sing_nosing_resolution = {'sing': [], 'nosing': []}
        lables_dir = '../fin_data_svd/%s/tags' % data_dir
        sing_num = 0
        nosing_num = 0
        sing_total = 0
        nosing_total = 0
        for item in os.listdir(lables_dir):
            lab_path = os.path.join(lables_dir, item)
            with open(lab_path, 'r') as laF:
                lab_cont = laF.readlines()
            for line_item in lab_cont:
                start_end = line_item.split(' ')
                if len(start_end) == 3:
                    start = float(start_end[0])
                    end = float(start_end[1])
                    tag = start_end[2].strip('\n')
                    sing_nosing_resolution[tag].append(end - start)
                    if tag == 'sing':
                        sing_total += 1
                        if (end - start) < 0.3:
                            sing_num += 1
                    elif tag == 'nosing':
                        nosing_total += 1
                        if (end - start) < 0.3:
                            nosing_num += 1

        plt.figure()
        ax0 = plt.subplot(211)
        plt.gca().set_title('sing |<0.3s %d of %d' % (sing_num, sing_total))
        plt.axis([0, 0.5, 0, 100])
        plt.hist(sing_nosing_resolution['sing'], bins=2000)
        plt.subplot(212, sharex=ax0)
        plt.gca().set_title('nosing|<0.3s %d  of %d' % (nosing_num, nosing_total))
        plt.axis([0, 0.5, 0, 100])
        plt.hist(sing_nosing_resolution['nosing'], bins=2000)
        plt.subplots_adjust(hspace=1.0)
        plt.savefig('%s_sing_nosing_time_dur.png' % data_dir)
        return sing_nosing_resolution


if __name__ == '__main__':
    for data_dir in ['Jamendo', 'RWC']:  # , 'Jamendo','MIR1K', 'iKala', 'MedleyDB']:
        for ext_str in [0.05]:  # , 0.10, 0.20, 0.30, 0.50, 0.80, 1, 2]:  # , 3]:# 3s is too long to all empty data
            data_set_dir = '../fin_data_svd/%s' % data_dir
            data_ext_str = '_%.2f.joblib' % ext_str
            for vocal_only in [False]:  # True, False]:
                dataset_saver = Dataset_save_gen()
                dataset_saver.writeDataset2H5file(data_set_dir, data_ext_str, vocal_only)
