import matplotlib.pyplot as plt
from scipy.io.wavfile import read as wavread

fs,data=wavread('../../test.wav')
raw_plot_data=data[8000:16000]
frame_size=50/1000*fs
framed_plot_data=[]
for item in range(len(raw_plot_data)):
    if item%frame_size==0:
        framed_plot_data.extend([0]*200)
    else:framed_plot_data.append(raw_plot_data[item])

fig1 = plt.figure()
plt.title(' ')
# plt.plot(framed_plot_data)
plt.plot(raw_plot_data,'red')
plt.xticks([])
plt.yticks([])
ax = plt.gca()
ax.spines['top'].set_visible(False) #去掉上边框
ax.spines['bottom'].set_visible(False) #去掉下边框
ax.spines['left'].set_visible(False) #去掉左边框
ax.spines['right'].set_visible(False) #去掉右边框


plt.savefig(fname="name.svg",format="svg")
plt.savefig(fname="name.png",format="png")
plt.show()