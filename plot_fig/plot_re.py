import matplotlib.pyplot as plt
from scipy.io.wavfile import read as wavread

fs, data = wavread('../../test.wav')


def plot_3s_wav_25ms_wav_in_fig_a_b():
    raw_plot_data = data[fs:fs * 4]
    frame_size = int(25  * fs / 1000) # 25ms

    plt.figure()

    plt.subplot(211)
    plt.title('a')
    plt.plot(raw_plot_data, 'blue')
    plt.vlines(frame_size * 20, -30000, 30000, colors = "r", linestyles = "dashed")
    plt.vlines(frame_size * 21, -30000, 30000, colors="r", linestyles="dashed")
    plt.subplot(212)
    plt.title('b')
    plt.plot(raw_plot_data[frame_size * 20:frame_size * 21], 'red')

    # plt.xticks([])
    # plt.yticks([])
    # ax = plt.gca()
    # ax.spines['top'].set_visible(False)  # 去掉上边框
    # ax.spines['bottom'].set_visible(False)  # 去掉下边框
    # ax.spines['left'].set_visible(False)  # 去掉左边框
    # ax.spines['right'].set_visible(False)  # 去掉右边框

    plt.savefig(fname="3s_wav_25ms_wav_in_fig_a_b.svg", format="svg")
    plt.savefig(fname="3s_wav_25ms_wav_in_fig_a_b.png", format="png")
    plt.show()

    return 0


if __name__ == '__main__':
    plot_3s_wav_25ms_wav_in_fig_a_b()
