import numpy as np
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score


class Evaluator_Parm:
    def __init__(self, pre, tru):
        self.pre = pre
        self.tru = tru

    def get_acc(self):
        right = 0
        total = 0
        for pr, tr in zip(self.pre, self.tru):
            total += 1
            if pr == tr:
                right += 1
        self.acc = right / 1.0 / total
        return right / 1.0 / total

    def get_evaluation_f(self, neg=0):
        total = len(self.tru)
        TP = 0
        TN = 0
        FP = 0
        FN = 0
        '''
        TP:method says its positive and ground truth agrees
        TN:method says its negative and ground truth agrees
        FP:method says its positive  ground truth disagrees
        FN method says its negative ground truth disagrees
        '''
        for pre, tru in zip(self.pre, self.tru):
            if (pre == tru and pre == 1 - neg):
                TP += 1
            elif (pre == tru and pre == neg):
                TN += 1
            elif pre != tru and pre == 1 - neg:
                FP += 1
            elif pre != tru and pre == neg:
                FN += 1
            else:
                raise ("error...%s-%s" % (pre, tru))

        self.accuracy = (TP + TN) / 1.0 / total
        self.precision = TP / 1.0 / (TP + FP)
        self.recall = TP / 1.0 / (TP + FN)
        self.specificity = TN / 1.0 / (TN + FP)
        self.f1measure = 2 * self.precision * self.recall / 1.0 / (self.precision + self.recall)
        return self.accuracy, self.precision, self.recall, self.f1measure  # , self.specificity

    def get_metrics_f(self):
        self.pre = self.pre.reshape(-1)
        try:
            self.tru = self.tru.reshape(-1)
        except:
            pass
        acc = accuracy_score(self.pre, self.tru)
        pre = precision_score(self.pre, self.tru)
        rec = recall_score(self.pre, self.tru)
        f1 = f1_score(self.pre, self.tru)

        self.pre = np.array([1] * len(self.pre)) - self.pre
        self.tru = np.array([1] * len(self.pre)) - self.tru
        acc_neg = accuracy_score(self.pre, self.tru)
        pre_neg = precision_score(self.pre, self.tru)
        rec_neg = recall_score(self.pre, self.tru)
        f1_neg = f1_score(self.pre, self.tru)
        acc = (acc + acc_neg) / 2
        pre = (pre + pre_neg) / 2
        rec = (rec + rec_neg) / 2
        f1 = (f1 + f1_neg) / 2

        return acc, pre, rec, f1


if __name__ == '__main__':
    ep = Evaluator_Parm([1, 0, 1, 0], [1, 0, 1, 1])
    print(ep.get_metrics_f())
    print(ep.get_evaluation_f())
