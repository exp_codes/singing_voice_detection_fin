# coding=utf-8
import os
from matplotlib import pyplot as plt
import joblib
import numpy
import numpy as np
from keras import Model
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.layers import Dense, Conv1D, MaxPooling1D, Flatten, LSTM, GRU, Bidirectional, TimeDistributed, \
    LeakyReLU, Dropout, Reshape
from keras.models import Sequential, load_model
from keras.optimizers import Adam
from keras.utils import plot_model
from sklearn.ensemble import RandomForestClassifier

from config import retrain, model_saved_path, model_weights_saved_path, EARLY_STOP_STEP, \
    valid_steps, tb_log_dir, train_steps, cp_save_path, batch_size, load_model_path, VERBOSE, rmse, contrast, \
    feat_g_delta, feat_g_delta_delta, feat_g_mfcc, \
    feat_g_plp, feat_g_spec, SEED, feat_g_lpcc, feat_g_chroma, zcr, rolloff, polly, bandwidth, flatness, \
    time_step, time_steps, dropout_rate
from preprocess import batch_data_gen, get_evaluation_f, load_Dataset_from_h5file, filter_nan, use_selc_feat


def build_load_deep_model(deep_model, time_steps, input_size):
    if retrain or not os.path.isfile(model_saved_path):
        if deep_model == 'dnn':
            model = Sequential()
            model.add(Dense(32, activation='relu', input_shape=(input_size,)))
            model.add(Dropout(dropout_rate))
            model.add(Dense(16, activation='relu'))
            model.add(Dropout(dropout_rate))
            model.add(Dense(1, activation='softmax'))
        elif deep_model == 'cnn':
            model = Sequential()
            model.add(Reshape((input_size, 1), input_shape=(input_size,)))
            model.add(
                Conv1D(64, 3, name='conv1', padding='valid', kernel_initializer='he_normal',
                       ))
            model.add(LeakyReLU(0.01))
            model.add(Conv1D(32, 3, name='conv2', padding='valid', kernel_initializer='he_normal'))
            model.add(LeakyReLU(0.01))
            model.add(MaxPooling1D(3, strides=3))

            model.add(Conv1D(128, 3, name='conv3', padding='valid', kernel_initializer='he_normal'))
            model.add(LeakyReLU(0.01))
            model.add(Conv1D(64, 3, name='conv4', padding='valid', kernel_initializer='he_normal'))
            model.add(LeakyReLU(0.01))
            model.add(MaxPooling1D(3, strides=3))

            model.add(Flatten())
            model.add(Dense(256))
            model.add(Dropout(dropout_rate))
            model.add(Dense(64))
            model.add(Dropout(dropout_rate))
            model.add(Dense(1, activation='sigmoid'))
        elif deep_model == 'lstm':
            model = Sequential()
            model.add(Bidirectional(LSTM(10, return_sequences=True), input_shape=(time_steps, input_size)))
            model.add(Bidirectional(LSTM(10, return_sequences=True)))
            model.add(Bidirectional(LSTM(10, return_sequences=True)))
            model.add(TimeDistributed(Dense(1, activation='sigmoid')))
        elif deep_model == 'gru':
            model = Sequential()
            model.add(Bidirectional(GRU(30, return_sequences=True), input_shape=(time_steps, input_size)))
            model.add(Bidirectional(GRU(20, return_sequences=True)))
            model.add(Bidirectional(GRU(40, return_sequences=True)))
            model.add(TimeDistributed(Dense(1, activation='softmax')))
    else:
        model = load_model(model_saved_path)
        model.load_weights(model_weights_saved_path)
    adamOP = Adam()
    model.compile(loss='binary_crossentropy',
                  optimizer=adamOP,  # 'adam',
                  metrics=['accuracy'])

    # print model.summary()

    return model


def train_deep_models(data_dir='', deep_model='dnn', dataset=''):
    print(deep_model)
    feat_selc = use_selc_feat([zcr,
                               rmse,
                               rolloff,
                               contrast,
                               polly,
                               flatness,
                               feat_g_lpcc,
                               feat_g_plp
                               ])  # ?
    input_size = len(feat_selc)
    model = build_load_deep_model(deep_model, time_steps, input_size, )
    if os.path.isfile(load_model_path) and not retrain:
        print('load model from %s' % load_model_path)
        model.load_weights(load_model_path)
    plot_model(model, to_file='models/%s.png' % deep_model, show_shapes=True)
    checkpoint = ModelCheckpoint(cp_save_path, monitor='loss', verbose=1, save_best_only=True, mode='min')
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=EARLY_STOP_STEP, verbose=0,
                               mode='auto', )  # baseline=None, restore_best_weights=True)

    train_test_valid = joblib.load(os.path.join(data_dir, dataset))
    train_lst = train_test_valid['train']
    valid_lst = train_test_valid['valid']
    test_lst = train_test_valid['test']
    if deep_model not in ['gru', 'lstm']:
        time_steps_item = 1
    else:
        time_steps_item = time_steps
    if retrain:
        history=model.fit_generator(batch_data_gen(train_lst, batch_size, feat_selc, time_steps_item),
                            steps_per_epoch=train_steps,
                            epochs=10000, verbose=VERBOSE,
                            shuffle=True,
                            validation_data=batch_data_gen(valid_lst, batch_size, feat_selc, time_steps_item),
                            validation_steps=valid_steps,
                            callbacks=[checkpoint, early_stop,
                                       TensorBoard(log_dir=tb_log_dir), ])
        plt.figure()
        plt.plot()
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig('val_acc.png')
        # summarize history for loss
        plt.figure()
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig('val_loss.png')

    # 已有的model在load权重过后
    # 取某一层的输出为输出新建为model，采用函数模型
    dense1_layer_model = Model(inputs=model.input,
                               outputs=model.layers[-2].output)
    # 以这个model的预测值作为输出dense1_layer_model.predict()

    return dense1_layer_model


def train_cnn_model_full_data_in(data_dir='../data/jamendo', model_name="CNN", frame_size='0.04'):
    feat_selc = use_selc_feat([zcr,
                               rmse,
                               rolloff,
                               contrast,
                               polly,
                               flatness,
                               feat_g_lpcc,
                               feat_g_plp
                               ])  # ?
    input_size = len(feat_selc)
    model = build_load_deep_model(time_steps, input_size)
    if os.path.isfile(load_model_path) and not retrain:
        print('load model from %s' % load_model_path)
        model.load_weights(load_model_path)
    plot_model(model, to_file='models/%s.png' % model_name, show_shapes=True)
    checkpoint = ModelCheckpoint(cp_save_path, monitor='loss', verbose=1, save_best_only=True, mode='min')
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=EARLY_STOP_STEP, verbose=0,
                               mode='auto', )  # baseline=None, restore_best_weights=True)

    trainX, trainY, testX, testY, validX, validY = load_Dataset_from_h5file(
        data_dir + '/train_test_valid_win_%ss.h5' % frame_size)
    trainX, trainY = filter_nan(trainX, trainY)
    testX, testY = filter_nan(testX, testY)
    validX, validY = filter_nan(validX, validY)
    f = lambda x: 1 if x == 'sing' else 0
    trainY = numpy.array([f(lable_y) for lable_y in trainY])
    validY = numpy.array([f(lable_y) for lable_y in validY])

    model.fit(trainX[:, feat_selc], trainY,
              epochs=10000, verbose=VERBOSE, batch_size=batch_size,
              shuffle=True,
              validation_data=(validX[:, feat_selc], validY),
              callbacks=[checkpoint, early_stop,
                         TensorBoard(log_dir=tb_log_dir), ])

    return 0


def use_RWC_train_jamendo_test():
    feat_selc = use_selc_feat([zcr,
                               rmse,
                               rolloff,
                               contrast,
                               polly,
                               flatness,
                               feat_g_lpcc,
                               feat_g_plp
                               ])  # ?
    input_size = len(feat_selc)
    model = build_load_deep_model(time_steps, input_size)
    if os.path.isfile(load_model_path) and not retrain:
        print('load model from %s' % load_model_path)
        model.load_weights(load_model_path)
    plot_model(model, to_file='models/model.png', show_shapes=True)
    checkpoint = ModelCheckpoint(cp_save_path, monitor='loss', verbose=1, save_best_only=True, mode='min')
    early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=EARLY_STOP_STEP, verbose=0,
                               mode='auto', )  # baseline=None, restore_best_weights=True)

    RWC_dict = joblib.load('../data/RWC/train_test_valid_win_0.30s.joblib')
    RWC_lst = []
    RWC_lst.extend(RWC_dict['train'])
    RWC_lst.extend(RWC_dict['valid'])
    RWC_lst.extend(RWC_dict['test'])
    jamendo_dict = joblib.load('../data/jamendo/train_test_valid_win_0.30s.joblib')
    jamendo_lst = []
    jamendo_lst.extend(jamendo_dict['train'])
    jamendo_lst.extend(jamendo_dict['valid'])
    jamendo_lst.extend(jamendo_dict['test'])

    if retrain:
        model.fit_generator(batch_data_gen(RWC_lst, batch_size, feat_selc, time_steps), steps_per_epoch=train_steps,
                            epochs=10000, verbose=VERBOSE,
                            shuffle=True,
                            validation_data=batch_data_gen(jamendo_lst, batch_size, feat_selc, time_steps),
                            validation_steps=valid_steps,
                            callbacks=[checkpoint, early_stop,
                                       TensorBoard(log_dir=tb_log_dir), ])

        # 取某一层的输出为输出新建为model，采用函数模型
        dense1_layer_model = Model(inputs=model.input,
                                   outputs=model.layers[-2].output)
        # 以这个model的预测值作为输出dense1_layer_model.predict()
        trainX, trainY, testX, testY, validX, validY = load_Dataset_from_h5file(
            '../data/jamendo/train_test_valid_win_0.30s.h5')
        trainX, trainY = filter_nan(trainX, trainY)
        testX, testY = filter_nan(testX, testY)
        validX, validY = filter_nan(validX, validY)
        f = lambda x: 1 if x == 'sing' else 0
        trainY = numpy.array([f(lable_y) for lable_y in trainY])
        testY = numpy.array([f(lable_y) for lable_y in testY])
        validY = numpy.array([f(lable_y) for lable_y in validY])
        trainX = dense1_layer_model.predict(trainX[:, feat_selc])
        testX = dense1_layer_model.predict(testX[:, feat_selc])
        validX = dense1_layer_model.predict(validX[:, feat_selc])
        return trainX, trainY, testX, testY, validX, validY


def sing_nosing_1_0(Y):
    new_Y = []
    for item in list(Y):
        if item == 'nosing':
            new_Y.append(0)
        else:
            new_Y.append(1)
    new_Y = np.array(new_Y)
    return new_Y


def just_clf(trainX, trainY, testX, testY, validX, validY):
    # trainX = normalize(trainX, axis=0, norm='max')
    # testX = normalize(testX, axis=0, norm='max')
    # validX = normalize(validX, axis=0, norm='max')
    model_path_saved = 'models/just_clf.model.joblib'
    if os.path.isfile(model_path_saved):
        clf = joblib.load(model_path_saved)
    else:
        clf = RandomForestClassifier(min_samples_leaf=20, random_state=SEED, n_estimators=100)

    clf = clf.fit(trainX, trainY)
    joblib.dump(clf, model_path_saved)
    preRes = clf.predict(validX)
    accuracy, precision, recall, f1measure, specificity = get_evaluation_f(preRes, validY)

    print ('%.3f\t%.3f\t%.3f\t%.3f\t%.3f' % (accuracy, precision, recall, f1measure, specificity))
    return accuracy, precision, recall, f1measure, specificity


def use_jamendo_rwc_clf(selc_feat=[]):
    for train_dataset_name in ['iKala', 'MIR-1K', 'jamendo', 'RWC']:
        for valid_dataset_name in ['iKala', 'MIR-1K', 'jamendo', 'RWC']:
            jamendo_trainX, jamendo_trainY, jamendo_testX, jamendo_testY, jamendo_validX, jamendo_validY = load_Dataset_from_h5file(
                '../data/%s/train_test_valid_win_0.30s.h5' % train_dataset_name)
            RWC_trainX, RWC_trainY, RWC_testX, RWC_testY, RWC_validX, RWC_validY = load_Dataset_from_h5file(
                '../data/%s/train_test_valid_win_0.30s.h5' % valid_dataset_name)
            jamendo__X = np.vstack((jamendo_trainX, jamendo_testX, jamendo_validX))
            jamendo__Y = np.concatenate((jamendo_trainY, jamendo_testY, jamendo_validY))
            RWC__X = np.vstack((RWC_trainX, RWC_testX, RWC_validX))
            RWC__Y = np.concatenate((RWC_trainY, RWC_testY, RWC_validY))
            trainX, trainY = filter_nan(jamendo__X, jamendo__Y)
            validX, validY = filter_nan(RWC__X, RWC__Y)

            # validX, validY = filter_nan(jamendo__X, jamendo__Y)
            # trainX, trainY= filter_nan(RWC__X, RWC__Y)

            feat_selc = use_selc_feat(selc_feat)
            clf = RandomForestClassifier(min_samples_leaf=20, random_state=SEED, n_estimators=100)

            clf = clf.fit(trainX[:, feat_selc], sing_nosing_1_0(trainY))
            joblib.dump(clf, 'models/svm.model.joblib')
            preRes = clf.predict(validX[:, feat_selc])
            accuracy, precision, recall, f1measure, specificity = get_evaluation_f(preRes, sing_nosing_1_0(validY))

            print ('%s|%s\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f' % (
                train_dataset_name, valid_dataset_name, accuracy, precision, recall, f1measure, specificity))
    return accuracy, precision, recall, f1measure, specificity


def use_SVM_clf(data_dir='../data/RWC', selc_feat=[], frame_size='0.04'):
    trainX, trainY, testX, testY, validX, validY = load_Dataset_from_h5file(
        data_dir + '/train_test_valid_win_%ss.h5' % frame_size)
    trainX, trainY = filter_nan(trainX, trainY)
    testX, testY = filter_nan(testX, testY)
    validX, validY = filter_nan(validX, validY)

    feat_selc = use_selc_feat(selc_feat)
    clf = RandomForestClassifier(min_samples_leaf=20, random_state=SEED, n_estimators=100)

    clf = clf.fit(trainX[:, feat_selc], sing_nosing_1_0(trainY))
    joblib.dump(clf, 'models/svm.model.joblib')
    preRes = clf.predict(validX[:, feat_selc])
    accuracy, precision, recall, f1measure, specificity = get_evaluation_f(preRes, sing_nosing_1_0(validY))

    print ('%.3f\t%.3f\t%.3f\t%.3f\t%.3f' % (accuracy, precision, recall, f1measure, specificity))
    return accuracy, precision, recall, f1measure, specificity


def compare_features_by_g(data_dir='../data/jamendo', frame_size='0.04'):
    full_feature_sets = [
        feat_g_delta_delta,
        feat_g_delta,
        feat_g_mfcc,
        feat_g_lpcc,
        feat_g_chroma,
        feat_g_spec,
        feat_g_plp

    ]

    for selcte_feat in (full_feature_sets):
        accuracy, precision, recall, f1measure, specificity = use_SVM_clf(data_dir, [selcte_feat], frame_size)

        with open('compare_features_by_g.txt', 'a') as df:
            df.write('accuracy\tprecision\trecall\tf1measure\tspecificity\n')
            df.write('%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n' % (accuracy, precision, recall, f1measure, specificity))
    return 0


def select_feature_by_add_in_higher(data_dir='../data/RWC', frame_size='0.30'):
    # feature_lst = [zcr,
    #                rmse,
    #                rolloff,
    #                centroid,
    #                contrast,
    #                polly,
    #                bandwidth,
    #                flatness,
    #                # sepc feat
    #                feat_g_lpcc,
    #                feat_g_delta_delta,
    #                feat_g_delta,
    #                feat_g_mfcc,
    #                feat_g_plp,
    #                feat_g_chroma]

    feature_lst = [zcr,
                   rmse,
                   rolloff,
                   contrast,
                   polly,
                   flatness,
                   feat_g_lpcc,
                   feat_g_plp
                   ]  # jamendo best features

    feature_lst = [zcr,
                   rmse,
                   rolloff,
                   contrast,
                   bandwidth,
                   feat_g_lpcc,
                   feat_g_plp
                   ]  # RWC best features

    now_feats = [feature_lst[0]]
    now_f1 = use_SVM_clf(selc_feat=now_feats, data_dir=data_dir, frame_size=frame_size)[-2]
    counter_item = 1
    for item in feature_lst[1:]:
        counter_item += 1
        add_item_f1 = use_SVM_clf(selc_feat=now_feats + [item], data_dir=data_dir, frame_size=frame_size)[-2]
        if add_item_f1 > now_f1:
            now_f1 = add_item_f1
            now_feats.append(item)
            print ('++++%d' % counter_item)
        else:
            print ('----%d' % counter_item)

    return 0


# -----------------------------------------------------------
def build_time_step_cnn(data_dir, frame_size):
    trainX, trainY, testX, testY, validX, validY = load_Dataset_from_h5file(
        data_dir + '/train_test_valid_win_%ss.h5' % frame_size)
    trainX, trainY = filter_nan(trainX, trainY)
    testX, testY = filter_nan(testX, testY)
    validX, validY = filter_nan(validX, validY)
    f = lambda x: 1 if x == 'sing' else 0
    trainY = numpy.array([f(lable_y) for lable_y in trainY])
    validY = numpy.array([f(lable_y) for lable_y in validY])

    feat_selc = use_selc_feat([zcr,
                               rmse,
                               rolloff,
                               contrast,
                               polly,
                               flatness,
                               feat_g_lpcc,
                               feat_g_plp
                               ])  # ?
    trainX = trainX[:, feat_selc]
    testX = testX[:, feat_selc]
    validX = validX[:, feat_selc]
    use_old_cnn_network(trainX, trainY, testX, testY, validX, validY)

    return 0


def use_old_cnn_network(trainX, trainY, testX, testY, validX, validY):
    trainX, trainY = get_step_data(trainX, trainY, time_step, array_dim_num=3)
    testX, testY = get_step_data(testX, testY, time_step, array_dim_num=3)
    validX, validY = get_step_data(validX, validY, time_step, array_dim_num=3)
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    feat_dim = numpy.shape(trainX)[-1]
    model = Sequential()
    model.add(Conv1D(4, 4, input_shape=(time_step, feat_dim), activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(4, 4, activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Flatten())
    model.add(Dense(1, activation='softmax', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    print model.summary()
    callbacks = [EarlyStopping(monitor='val_loss', patience=2, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=100000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE)
    print model.summary()
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(validX)
    print('gen report of evaluation...')
    # acc, pre, rec, f1, spe = get_evaluation_f(validY, predictY)
    return model  # , acc, pre, rec, f1, spe


def get_step_data(dataX, dataY, step, array_dim_num=3):
    feat_dim = numpy.shape(dataX)[-1]
    finalX = []
    finalY = []
    for item_y in range(0, len(dataY), step):
        if list(dataY[item_y:item_y + step]).count(1) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(1)
        if list(dataY[item_y:item_y + step]).count(0) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(0)
    finalX = numpy.array(finalX)
    finalY = numpy.array(finalY)
    if array_dim_num == 4:
        finalX = numpy.reshape(finalX, (-1, 1, 1, step, feat_dim))
    return finalX, finalY
