import contextlib
import wave
import webrtcvad
from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite
import os
from pydub import AudioSegment


def read_wave(path):
    """Reads a .wav file.
    Takes the path, and returns (PCM audio data, sample rate).
    """
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        num_channels = wf.getnchannels()
        assert num_channels == 1
        sample_width = wf.getsampwidth()
        assert sample_width == 2
        sample_rate = wf.getframerate()
        assert sample_rate in (8000, 16000, 32000)
        pcm_data = wf.readframes(wf.getnframes())
        return pcm_data, sample_rate


def write_wave(path, audio, sample_rate):
    """Writes a .wav file.
    Takes path, PCM audio data, and sample rate.
    """
    with contextlib.closing(wave.open(path, 'wb')) as wf:
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(sample_rate)
        wf.writeframes(audio)


class Frame(object):
    """Represents a "frame" of audio data."""

    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, audio, sample_rate):
    """Generates audio frames from PCM audio data.
    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.
    Yields Frames of the requested duration.
    """
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = 0.0
    duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += duration
        offset += n


def count_0_1_and_gen_timestamp(res_0_1, time_dur):
    timestamp = []
    point_last_time = time_dur
    now_0_1 = -1
    for index, item in enumerate(res_0_1):
        if item != now_0_1:
            if index != 0:
                timestamp.append([res_0_1[index - 1], point_last_time])
            now_0_1 = item
            point_last_time = time_dur
        else:
            point_last_time += time_dur
    timestamp.append([res_0_1[- 1], point_last_time])
    print(timestamp)
    return timestamp


def gen_label_according_timestamp(timstamp):
    label_cont = []
    end = 0
    for item in timstamp:
        lable = 'sing\n' if item[0] == 1 else 'nosing\n'
        start = end
        end += item[1]
        label_cont.append('%.3f %.3f %s' % (start / 1000.0, end / 1000.0, lable))
    print(label_cont)
    return label_cont


def detect_silent_voice_from_right_channel_of_vocal(wav_path='dua.wav'):
    vad = webrtcvad.Vad(3)
    frame_dur = 10  # ms
    try:
        sampleRate, audioData = wavread(wav_path)
        right_vocal_channel = audioData[:, 1]
        wavwrite(wav_path, sampleRate, right_vocal_channel)
    except:
        print('not dual channel')
    audio, sample_rate = read_wave(wav_path)
    frames = frame_generator(frame_dur, audio, sample_rate)
    frames = list(frames)
    flags_result = []
    for frame in frames:
        is_speech = vad.is_speech(frame.bytes, sample_rate)
        flags_result.append(1 if is_speech else 0)
    print(flags_result)
    print(len(flags_result) * frame_dur / 1000.0)
    timestamp = count_0_1_and_gen_timestamp(flags_result, frame_dur)
    lable_cont = gen_label_according_timestamp(timestamp)
    lab_file_path = wav_path.replace('.wav', '.lab').replace('Wavfile/', 'tags/')
    dir_lab = os.path.dirname(lab_file_path)
    if not os.path.isdir(dir_lab):
        os.makedirs(dir_lab)
    with open(lab_file_path, 'w') as lf:
        lf.writelines(lable_cont)
    return 0


def convert_to_16k(wavPath):
    sound = AudioSegment.from_wav(wavPath)
    sound = sound.set_frame_rate(16000)
    sound.export(wavPath, 'wav')
    return wavPath


def batchGenLable(wavDirs=['ikalaWavfile', 'mir1kWavfile']):
    for wavDir in wavDirs:
        for filename in os.listdir(wavDir):
            if '.wav' in filename:
                filePath = os.path.join(wavDir, filename)
                print(filePath, '...')
                filePath = convert_to_16k(filePath)
                detect_silent_voice_from_right_channel_of_vocal(filePath)
    return 0


if __name__ == '__main__':
    batchGenLable()
