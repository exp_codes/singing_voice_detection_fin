import os

import joblib
import soundfile
from pydub import AudioSegment

from extract_feat import AudioFeat
from unet_svs import split_it


class Pre_procession_wav:
    def __init__(self, dir_path):
        self.dir_path = dir_path
        self.DataX = []
        self.LabelY = []

    def format_wav(self):
        for root, dirs, names in os.walk(self.dir_path):
            for name in names:
                audio_path = os.path.join(root, name)

                if os.path.basename(audio_path).startswith('.'):
                    os.remove(audio_path)
                    continue
                format_audio = audio_path.split('.')[-1]
                if format_audio == 'lab':  # 跳过标签lable文件
                    continue
                song = AudioSegment.from_file(audio_path, format_audio)
                song = song.set_channels(1)
                song = song.set_frame_rate(16000)
                song = song.set_sample_width(2)
                wav_path = audio_path.replace('.' + format_audio, '.wav')
                os.remove(audio_path)
                song.export(wav_path, 'wav')
                print(wav_path)

    def svs_get_vocal(self):
        # https://gitlab.com/exp_codes/comparisonSVS
        for root, dirs, names in os.walk(self.dir_path):
            for item in names:
                pat_th = os.path.join(root, item)
                if '.wav' in item and '.unet.Vocal.wav' not in item:
                    vocal_path = pat_th.replace('.wav', '.unet.Vocal.wav')
                    print(vocal_path)
                    if not os.path.isfile(vocal_path):
                        split_it(pat_th)
                    # os.remove(pat_th)

    def extract_feat_from_wav(self, win_size, hop_size, vocal_only):
        for root, dirs, names in os.walk(self.dir_path):
            for name in names:
                DataX = []
                LabelY = []
                wav_path = os.path.join(root, name)
                if vocal_only:
                    if '.unet.Vocal.' not in wav_path:
                        continue
                wav_ext_str = '.' + wav_path.split('.')[-1]
                if wav_ext_str in '.lab.joblib':
                    continue  # 跳过lable
                feat_path = wav_path.replace('.wav', '_%.2f.joblib' % win_size)
                if os.path.isfile(feat_path):
                    continue  # 跳过已提取
                print(feat_path)
                signal, samplerate = soundfile.read(wav_path)
                win_size_num = int(win_size * samplerate)  # 0.01s to nums
                hop_size_num = int(hop_size * samplerate)
                if '/train/' in wav_path:
                    lab_path = wav_path.replace('/train/', '/tags/').replace(wav_ext_str, '.lab')
                elif '/test/' in wav_path:
                    lab_path = wav_path.replace('/test/', '/tags/').replace(wav_ext_str, '.lab')
                elif '/valid/' in wav_path:
                    lab_path = wav_path.replace('/valid/', '/tags/').replace(wav_ext_str, '.lab')
                if '.unet.Vocal.' in lab_path:
                    lab_path = lab_path.replace('.unet.Vocal.', '.')
                lab_contend = open(lab_path)
                lab_contend_lines = lab_contend.readlines()
                lab_contend.close()
                for line in lab_contend_lines:
                    line_item_list = line.split(' ')
                    start_time = float(line_item_list[0])
                    end_time = float(line_item_list[1])
                    lab_sing_nosing = line_item_list[2][:-1]
                    part_signal = signal[int(start_time * samplerate):int(end_time * samplerate)]
                    try:
                        audio_feater = AudioFeat(win_size_n=win_size_num, hop_size_n=hop_size_num)
                        part_signal_feat = audio_feater.get_audio_feature(part_signal, samplerate)
                        DataX.extend(part_signal_feat)
                        f = lambda x: 1 if x == 'sing' else 0
                        LabelY.extend([f(lab_sing_nosing)] * len(part_signal_feat))
                    except:
                        print('song:%s start:%s end:%s feat extract jump the part signals ...' % (
                        wav_path, start_time, end_time))
                joblib.dump({'data': DataX, 'lable': LabelY}, feat_path, compress=3)


if __name__ == '__main__':
    for dataset in ['Jamendo', 'RWC']:  # , 'MIR1K', 'iKala', 'MedleyDB']:
        dataset_dir_path = '../fin_data_svd/%s' % dataset
        preprocess_wav = Pre_procession_wav(dataset_dir_path)
        # preprocess_wav.format_wav()
        # preprocess_wav.svs_get_vocal()

        # for win_size in [0.05, 0.10, 0.20, 0.30, 0.50, 0.80, 1, 2, 3]:
        #     hop_size = 0.04
        #     preprocess_wav.extract_feat_from_wav(win_size, hop_size,True)

        preprocess_wav.extract_feat_from_wav(0.5, 0.04, False)
