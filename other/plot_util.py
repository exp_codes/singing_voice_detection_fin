# -*- coding: UTF-8 -*-
import matplotlib.pyplot as plt


class Fig_Ploter:
    def plot_multi_lines(self, fig_out_png_path, title_str, xlable_str, ylabel_str, x_axis, y_axis, labels):
        # 开始画图
        plt.title(title_str)
        for item in range(len(labels)):
            plt.plot(x_axis[item], y_axis[item], label=labels[item])
        plt.legend()  # 显示图例
        plt.xlabel(xlable_str)
        plt.ylabel(ylabel_str)
        plt.savefig(fig_out_png_path)
        foo_fig = plt.gcf()  # 'get current figure'
        foo_fig.savefig(fig_out_png_path.replace('.png', '.eps'), format='eps', dpi=1000)
        plt.show()


if __name__ == '__main__':
    fig_plt = Fig_Ploter()
    fig_plt.plot_multi_lines('test.png', 'test', 'num', 'fre', [[1, 2, 3], [1, 2, 3], [4, 5, 6]],
                             [[4, 4, 4], [6, 7, 6], [0, 8, 8]], ['a', 'b', 'c'])
