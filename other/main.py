# coding=utf-8
import warnings
from time import time

warnings.simplefilter("ignore")
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import joblib
import numpy as np

from CNNmodel import train_deep_models, use_SVM_clf, compare_features_by_g, \
    select_feature_by_add_in_higher, just_clf, use_RWC_train_jamendo_test, build_time_step_cnn, use_jamendo_rwc_clf
from config import SEED, batch_size
from config import contrast, \
    feat_g_plp, feat_g_lpcc, zcr, rolloff, polly, flatness
from config import rmse
from preprocess import batch_extract_feat, gen_train_test_valid, save_data_x_y, svs_get_vocal, \
    split_dataset_mir1k_ikala_to_train_test_valid, view_label_resolution

np.random.seed(SEED)
process_str = '6'
print process_str
time_start = time()
if '1' in process_str:  # 歌声分离预处理获取人声部分
    for data_dir in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        svs_get_vocal('../data/%s' % data_dir)
    svs_get_vocal('../data/RWC')
if '2' in process_str:  # 从音频wav文件提取音频特征保存为joblib
    for data_dir in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        batch_extract_feat('../data/%s' % data_dir,
                           '.unet.Vocal.wav')  # 按照后缀提取，其中分帧时长见config Frame_size 0.04 0.10 0.20 0.30
if '3' in process_str:  # 从数据集生成用于训练模型的  数据集  train test valid  文件列表
    for wav_ext_str in ['_win_0.04s.feat.joblib', '_win_0.10s.feat.joblib', '_win_0.20s.feat.joblib',
                        '_win_0.30s.feat.joblib']:
        for data_dir in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
            gen_train_test_valid('../data/%s' % data_dir, wav_ext_str)
if '4' in process_str:  # 检查数据集中样本分布
    for data_dir in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        train_test_valid = joblib.load('../data/%s' % data_dir + '/train_test_valid_win_0.30s.joblib')
        for item_lst in ['train', 'test', 'valid']:
            ds_lst = train_test_valid[item_lst]
            tot = 0  # 0.04s train 356103 test 96355 valid 90625
            for item in ds_lst:
                data_feat = joblib.load(item)
                tot += len(data_feat)
            print data_dir, item_lst, tot, batch_size, int(tot / batch_size)
if '5' in process_str:  # 保存数据集xy用来sklearn中传统模型
    for data_dir_item in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        for frame_size in ['0.04', '0.10', '0.20', '0.30']:
            data_saved_train_test_valid_path = '../data/%s/train_test_valid_win_%ss.joblib' % (
                data_dir_item, frame_size)
            save_data_x_y(data_saved_train_test_valid_path, '../data/%s/' % data_dir_item)
if '6' in process_str:  # 使用划分好的数据进行训练网络
    for data_dir in ['RWC']:  # 'iKala', 'MIR-1K', 'jamendo',
        for modle_name in ['lstm', ]:  # "dnn", 'gru','cnn'
            train_deep_models(data_dir='../data/%s' % data_dir,
                              dataset='train_test_valid_win_0.04s.joblib', deep_model=modle_name)
            # just_clf(trainX, trainY, testX, testY, validX, validY)
if '7' in process_str:  # 使用sklearn传统模型测试特征
    jamendo_feature_lst = [zcr,
                           rmse,
                           rolloff,
                           contrast,
                           polly,
                           flatness,
                           feat_g_lpcc,
                           feat_g_plp
                           ]  # jamendo best features

    # RWC_feature_lst = [zcr,
    #                    rmse,
    #                    rolloff,
    #                    contrast,
    #                    bandwidth,
    #                    feat_g_lpcc,
    #                    feat_g_plp
    #                    ]  # RWC best features
    for data_dir in ['jamendo', 'RWC', 'iKala', 'MIR-1K']:
        use_SVM_clf(
            data_dir='../data/%s' % data_dir, selc_feat=jamendo_feature_lst, frame_size='0.30')
        # use_SVM_clf(
        #     data_dir='../data/%s' % data_dir, selc_feat=RWC_feature_lst, frame_size='0.30')
if '8' in process_str:  # 特征按照验证集上的性能排序逐个添加以获得最好特征组合
    for data_dir_item in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        select_feature_by_add_in_higher('../data/%s' % data_dir_item)
if '9' in process_str:  # 对比各个特征在验证集上使用skleran传统模型的效果
    for data_dir_item in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        for frame_size in ['0.30']:
            print ('data_dir:%s frame_size:%s' % (data_dir_item, frame_size))
            compare_features_by_g('../data/%s' % data_dir_item, frame_size)
if 'A' in process_str:
    trainX, trainY, testX, testY, validX, validY = use_RWC_train_jamendo_test()
    just_clf(trainX, trainY, testX, testY, validX, validY)
if 'B' in process_str:
    for data_dir_item in ['jamendo', 'RWC', 'MIR-1K', 'iKala']:
        for frame_size in ['0.04', '0.10', '0.20', '0.30']:
            build_time_step_cnn('../data/%s/' % data_dir_item, frame_size)
if 'C' in process_str:  # 对mir1k以及ikala数据集进行划分
    split_dataset_mir1k_ikala_to_train_test_valid('../data/MIR-1K/Wavfile')
if 'D' in process_str:
    for data_dir in ['iKala', 'MIR-1K', 'jamendo', 'RWC']:
        view_label_resolution(data_dir)

if 'E' in process_str:
    RWC_feature_lst = [zcr,
                       rmse,
                       rolloff,
                       contrast,
                       polly,
                       flatness,
                       feat_g_lpcc,
                       feat_g_plp
                       ]  # RWC best features
    use_jamendo_rwc_clf(selc_feat=RWC_feature_lst)

time_end = time()
print "it takes %.2f s" % (time_end - time_start)
