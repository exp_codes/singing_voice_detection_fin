import joblib
from preprocess import mid_filter, hmm_smoothing, get_acc, ndarrayPreY2listPryeY, get_evaluation_f
import matplotlib.pyplot as plt
from crf_smoothing import crf_smoothing
import numpy as np
preYpath = 'temp_out_data/CNN_rwc_preY.data'
tesYpath = 'temp_out_data/CNN_rwc_testY.data'
pre_probYpath = 'temp_out_data/CNN_rwc_predictY_prob.data'
prevalidYpath,validYpath='temp_out_data/CNN_rwc_prevalidY.data','temp_out_data/CNN_rwc_validY.data'
preY = joblib.load(preYpath)
tesY = joblib.load(tesYpath)
pre_probY = joblib.load(pre_probYpath)



preY = ndarrayPreY2listPryeY(preY)
mid_preY = mid_filter(list(preY), 3)
hmm_preY = abs(hmm_smoothing(pre_probY))

crf_preY = abs(np.array(crf_smoothing(preYpath, tesYpath,prevalidYpath,validYpath)))

print len(crf_preY),len(hmm_preY),len(mid_preY),len(preY),len(tesY)
print get_evaluation_f(tesY[:2000],preY[:2000])
print get_evaluation_f(tesY[:2000],mid_preY[:2000])
print get_evaluation_f(tesY[:2000],hmm_preY[:2000])
print get_evaluation_f(tesY[:2000],crf_preY[:2000])

fig=plt.figure()
ax1=fig.add_subplot(511)
ax2=fig.add_subplot(512)
ax3=fig.add_subplot(513)
ax4=fig.add_subplot(514)
ax5=fig.add_subplot(515)
ax1.plot(tesY[:2000])
ax1.set_title('a) ground trust')
ax2.plot(preY[:2000])
ax2.set_title('b) prediction')
ax3.plot(mid_preY[:2000])
ax3.set_title('c) median filter')
ax4.plot(hmm_preY[:2000])
ax4.set_title('d) HMM')
ax5.plot(crf_preY[:2000])
ax5.set_title('e) CRF')
fig.suptitle('segment comparison')
fig.subplots_adjust(hspace=1.3)
plt.show()
