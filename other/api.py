# coding=utf-8
import os
import numpy
import joblib
from matplotlib import pyplot as plt
from tqdm import tqdm

from config import zcr, rmse, rolloff, contrast, feat_g_lpcc, feat_g_plp, polly, flatness
from extractFeature import get_audio_feature
from preprocess import use_selc_feat
from unet_svs import split_it
import math
def replace_nan(dataX):
    dataX = numpy.float32(dataX)
    finalX = []
    for feature in dataX:
        for number in feature:
            if math.isinf(number) or math.isnan(number):
                feature=[0]*len(feature)
            finalX.append(feature)
    return numpy.array(finalX, dtype='float32')

def use_clf_detect_vocal(wav_path):
    # 歌声分离
    vocal_path = wav_path.replace('.wav', '.unet.Vocal.wav')
    if not os.path.isfile(vocal_path):
        split_it(wav_path)
    # 提取特征
    feat_path = vocal_path.replace('.wav', '_win_0.30s.feat.joblib')
    if not os.path.isfile(feat_path):
        dataX = get_audio_feature(test_wav=vocal_path)
        joblib.dump(dataX, feat_path)
    feat_data = joblib.load(feat_path)
    feat_selc = [zcr,
                 rmse,
                 rolloff,
                 contrast,
                 polly,
                 flatness,
                 feat_g_lpcc,
                 feat_g_plp
                 ]
    feat_selc = use_selc_feat(feat_selc)
    # clf加载
    model_path = "models/svm.model.joblib"
    clf = joblib.load(model_path)
    feat_data=numpy.vstack(feat_data)
    feat_data=replace_nan(feat_data)
    feat_data=numpy.nan_to_num(feat_data)

    preRes=clf.predict(feat_data[:, feat_selc])


    return preRes

def get_jamendo_rwc_tags_lab(lab_path):
    lable_lst = []
    with open(lab_path, 'r') as lp:
        lp_cont = lp.readlines()
    for item in lp_cont:
        res_line = item.strip('\n').split(' ')
        if len(res_line)==3:
            for time_item in range(int(float(res_line[0])*1000),int(float(res_line[1])*1000),10):
                if res_line[2]=='sing':
                    lable_lst.append(1)
                else:
                    lable_lst.append(0)
    return lable_lst

def api_wav_jamendo_rwc_lab_300_10(wav_path):

    preRes = use_clf_detect_vocal(wav_path)
    wav_ext_str='.wav'
    if '/train/' in wav_path:
        lab_path = wav_path.replace('/train/', '/tags/').replace(wav_ext_str, '.lab')
    elif '/test/' in wav_path:
        lab_path = wav_path.replace('/test/', '/tags/').replace(wav_ext_str, '.lab')
    elif '/valid/' in wav_path:
        lab_path = wav_path.replace('/valid/', '/tags/').replace(wav_ext_str, '.lab')
    else:
        lab_path = ''
        raise ('error in wav path')
    lable_lst = get_jamendo_rwc_tags_lab(lab_path)
    print (len(preRes),len(lable_lst),len(preRes)/1.0/len(lable_lst))
    plt.figure()
    plt.subplot(211)
    plt.plot(preRes)
    plt.subplot(212)
    plt.plot(lable_lst)
    plt.savefig(wav_path.replace('.wav', '.png'))

    return 0
def jamendo_rwc_use_demo(wav_dir):
    for root, dirs, names in os.walk(wav_dir):
        for name in tqdm(names):
            wav_path = os.path.join(root, name)
            if '.wav' in wav_path and '.unet.Vocal' not in wav_path:
                api_wav_jamendo_rwc_lab_300_10(wav_path)

    return 0

if __name__ == '__main__':
    jamendo_rwc_use_demo('../data/jamendo')

